#include <iostream>
#include "jpge.h"
#include "jpgd.h"

int main() {
  int width, height, comps;
  auto data = jpgd::decompress_jpeg_image_from_file("src.jpg", &width, &height, &comps, 4);
  jpge::compress_image_to_jpeg_file("hello.jpg", width, height, comps, data);
}
