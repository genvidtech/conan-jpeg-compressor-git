from conans import ConanFile, CMake, tools
import os
import shutil

class JpegcompressorConan(ConanFile):
    name = "jpeg-compressor"
    version = "104"
    license = "Public Domain"
    url = "https://bitbucket.org/genvidtech/conan-jpeg-compressor"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports = "CMakeLists.txt"

    def source(self):
        self.run("git clone https://github.com/richgel999/jpeg-compressor")
        self.run("cd jpeg-compressor && git checkout v%s" % self.version)
        shutil.copy("CMakeLists.txt", "jpeg-compressor/CMakeLists.txt")

    def build(self):
        cmake = CMake(self.settings)
        self.run('cmake jpeg-compressor %s' % (cmake.command_line,))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="jpeg-compressor")
        self.copy("*jpge.lib", dst="lib", keep_path=False)
        self.copy("*jpge.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["jpge"]
